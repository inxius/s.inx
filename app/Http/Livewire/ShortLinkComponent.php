<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ShortLink;
use Illuminate\Support\Facades\DB;
use Tuupola\Base62 as TBase;
use Illuminate\Support\Carbon;

class ShortLinkComponent extends Component
{
    public $url_string;
    public $encoded_url = false;
    public $err_message = false;

    protected $rules = [
        'url_string' => ['required', 'url'],
    ];

    public function shorting()
    {
        $this->validate();

        try {
            DB::beginTransaction();

            if ($link = ShortLink::firstWhere('url', $this->url_string)) {
                $link->deleted_at = Carbon::now()->addDay(3)->hour(0)->minute(0)->second(0);
                $link->save();
            } else {
                $link = new ShortLink();
                $link->url = $this->url_string;
                $link->deleted_at = Carbon::now()->addDay(3)->hour(0)->minute(0)->second(0);
                $link->save();
            }

            $encoding = new TBase();
            $encoded = $encoding->encode('B62'.$link->id);
            $this->encoded_url = "https://s.inx.my.id/".$encoded;

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            $this->err_message = 'Proses Gagal!';
        }
    }

    public function close()
    {
        $this->encoded_url = false;
        $this->url_string = "";
    }

    public function render()
    {
        return view('livewire.short-link-component');
    }

}
