<div class="p-5 sm:px-20 md:px-30 lg:px-32">
    <p class="text-5xl text-stone-600 font-bold text-center pb-5">s.inx</p>
    <div class="flex flex-col justify-center items-center gap-5 p-3 border-solid border-2 border-stone-200 rounded-md shadow-sm">
        <p class="font-bold text-xl text-stone-700 text-center">Paste the URL to be shortened</p>
        <form wire:submit.prevent='shorting' action="" class="w-full flex flex-col justify-center items-center gap-3">
            <input wire:model='url_string' type="url" name="" id="" placeholder="Paste Your URLS here..." class="border-solid border-2 border-sky-500 focus:outline-sky-800 w-full h-10 text-xl rounded-md px-2">
            <button type="submit" class="group flex flex-row justify-center items-center py-2 px-4 bg-sky-600 w-fit h-fit rounded-md hover:bg-sky-700">
                <svg wire:loading class="animate-spin h-7 w-7 -ml-3" viewBox="0 0 100 100">
                    <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50"></path>
                </svg>

                <p class="font-bold text-sky-100 group-hover:text-sky-200">Process</p>
            </button>
        </form>
        <div>
            <p class="text-center text-sm font-extralight">s.inx is a free tool to shorten URLs and generate short links</p>
            <p class="text-center text-sm font-extralight">Our URL shortener allows to create a shortened link making it easy to share</p>
            @if ($err_message)
                <p class="text-center text-sm font-normal">{{$err_message}}</p>
            @endif
        </div>
    </div>

    @if ($encoded_url)
        <div class="relative mt-5 p-5 border-solid border-2 border-stone-200 rounded-md shadow-sm">
            <button type="button" wire:click='close' class="absolute top-0 right-0 bg-slate-200 rounded-bl-lg px-2 hover:bg-slate-300 hover:font-bold">x</button>
            <p class="text-3xl text-stone-600 font-bold text-left pb-5">Your Shortened URLS</p>
            <a href={{$encoded_url}} target="_blank" rel="noopener noreferrer" class="text-lg font-bold text-blue-800">{{$encoded_url}}</a>
        </div>
    @endif

    <div class="mt-5 grid grid-cols-2 gap-1 justify-items-center lg:grid-cols-4">
        <div class="grid grid-flow-row auto-rows-auto md:grid-flow-row md:auto-rows-auto md:gap-2 md:w-64 justify-items-center content-start p-3 md:p-3 border-solid border-2 border-stone-200 hover:border-stone-300 rounded-md shadow-sm">
            <img src={{ url('icon-like.png') }} alt="">
            <div class="flex flex-col justify-center items-center">
                <p class="text-base font-bold text-center">Easy</p>
                <p class="place-self-center font-light text-sm text-justify md:text-center text-stone-700">s.inx is easy and fast, enter the long link to get your shortened link</p>
            </div>
        </div>

        <div class="grid grid-flow-row auto-rows-auto md:grid-flow-row md:auto-rows-auto md:gap-2 md:w-64 justify-items-center content-start p-3 md:p-3 border-solid border-2 border-stone-200 hover:border-stone-300 rounded-md shadow-sm">
            <img src={{ url('icon-url.png') }} alt="">
            <div class="flex flex-col justify-center items-center">
                <p class="text-base font-bold text-center">Shortened</p>
                <p class="place-self-center font-light text-sm text-justify md:text-center text-stone-700">Use any link, no matter what size, ShortURL always shortens</p>
            </div>
        </div>

        <div class="grid grid-flow-row auto-rows-auto md:grid-flow-row md:auto-rows-auto md:gap-2 md:w-64 justify-items-center content-start p-3 md:p-3 border-solid border-2 border-stone-200 hover:border-stone-300 rounded-md shadow-sm">
            <img src={{ url('icon-responsive.png') }} alt="">
            <div class="flex flex-col justify-center items-center">
                <p class="text-base font-bold text-center">Devices</p>
                <p class="place-self-center font-light text-sm text-justify md:text-center text-stone-700">Compatible with smartphones, tablets and desktop</p>
            </div>
        </div>

        <div class="grid grid-flow-row auto-rows-auto md:grid-flow-row md:auto-rows-auto md:gap-2 md:w-64 justify-items-center content-start p-3 md:p-3 border-solid border-2 border-stone-200 hover:border-stone-300 rounded-md shadow-sm">
            <img src={{ url('icon-secure.png') }} alt="">
            <div class="flex flex-col justify-center items-center">
                <p class="text-base font-bold text-center">Secure</p>
                <p class="place-self-center font-light text-sm text-justify md:text-center text-stone-700">Your URLS will be stored for 3 days and will be deleted automatically after that</p>
            </div>
        </div>
    </div>
</div>
