<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite('resources/css/app.css')
    <title>Document</title>
</head>
<body>
    <main>
        <div class="w-screen min-h-screen flex flex-col justify-center items-center gap-5">
            <p>Klik tombol dibawah untuk menlanjutkan :)</p>
            <a href={{$url_string}} class="" target="_blank" rel="noopener noreferrer">
                <button type="button" class="h-10 w-20 text-white rounded-lg bg-red-500 hover:bg-red-600">Lanjut</button>
            </a>
        </div>
    </main>
</body>
</html>