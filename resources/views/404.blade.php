<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @vite('resources/css/app.css')
    <title>Not Found</title>
</head>
<body>
    <div class="h-screen flex justify-center items-center">
        <div class="grid grid-cols-4 divide-x-2 w-fit">
            <p class="justify-self-end place-self-center p-3 text-2xl text-stone-800 font-bold">404</p>
            <p class="justify-self-start place-self-center p-3 col-span-3 text-base text-stone-500 font-light">This page could not be found.</p>
        </div>
    </div>
</body>
</html>